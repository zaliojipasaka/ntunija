(function ($) {
    Drupal.behaviors.remembered = {
        attach: function (context, settings) {
            $('.view-field-remember', context).click(function (e) {
                e.stopPropagation();
                e.preventDefault();

                var nid = $(this).data('nid');
                var url = '';
                var button = $(this);
                var buttonHtml = button.html();
                if(button.hasClass('remembered')) {
                    url = '/remembered/forget/' + nid;
                    button.removeClass('remembered');
                    button.html(buttonHtml.replace('Pamiršti', 'Įsiminti'));
                } else {
                    url = '/remembered/remember/' + nid;
                    button.addClass('remembered');
                    button.html(buttonHtml.replace('Įsiminti', 'Pamiršti'));
                }
                $.ajax({
                    url: url,
                    success: function(data){
                        $('#block-block-1').find('strong').html(data['count']);
                    },
                    dataType: 'json'
                });
            });
            $('.view-field-remember', context).each(function(){
                var button = $(this);
                var buttonHtml = button.html();
                if(Drupal.settings.remembered.objects.indexOf(button.data('nid').toString()) != -1) {
                    button.addClass('remembered');
                    button.html(buttonHtml.replace('Įsiminti', 'Pamiršti'));
                }
            });
        }
    };
})(jQuery);
