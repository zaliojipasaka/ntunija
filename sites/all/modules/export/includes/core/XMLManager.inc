<?php

/**
 * Class XMLManager
 */
class XMLManager {
    const TYPE_ALIO      = 'alio';
    const TYPE_DOMOPLIUS = 'domoplius';

    protected $types =  array(
        1 => 'nekilnojamas-turtas/butai/parduoda',
        2 => 'nekilnojamas-turtas/namai-sodybos-kotedzai/parduoda',
        3 => 'nekilnojamas-turtas/komercines-patalpos/parduoda',
        4 => 'nekilnojamas-turtas/butai/nuomoja',
        5 => 'nekilnojamas-turtas/namai-sodybos-kotedzai/nuomoja',
        6 => 'nekilnojamas-turtas/komercines-patalpos/nuomoja',
        11 => 'nekilnojamas-turtas/sklypai-miskai/parduoda',
        9 => 'nekilnojamas-turtas/namai-sodybos-kotedzai/parduoda',
        12 => 'nekilnojamas-turtas/butai/nuomoja',
        13 => 'nekilnojamas-turtas/garazai/parduoda'
    );

    protected $actions =  array(
        1 => 'Parduoda',
        2 => 'Parduoda',
        3 => 'Parduoda',
        4 => 'Nuomoja',
        5 => 'Nuomoja',
        6 => 'Nuomoja',
        11 => 'Parduoda',
        9 => 'Parduoda',
        12 => 'Nuomoja',
        13 => 'Parduoda'
    );

    public function getTypeById($typeId) {
        return $this->types[$typeId];
    }

    public function getActionById($typeId) {
        return $this->actions[$typeId];
    }

    public function startXML() {
        return '<?xml version="1.0"?>
        <skelbimai>';
    }

    public function endXML() {
        return '</skelbimai>';
    }

    public function getXMLFromNode($node, $exportType) {
        switch((int)$node->field_type[$node->language][0]['value']) {
            case 1:
                return $this->getButasParduodaXML($node, $exportType);
            case 2:
                return $this->getNamasParduodaXML($node, $exportType);
            case 3:
                return $this->getPatalposParduodaXML($node, $exportType);
            case 4:
                return $this->getButasNuomojaXML($node, $exportType);
            case 5:
                return $this->getNamasNuomojaXML($node, $exportType);
            case 6:
                return $this->getPatalposNuomojaXML($node, $exportType);
            case 11:
                return $this->getSklypaiParduodaXML($node, $exportType);
            case 9:
                return $this->getSodybosParduodaXML($node, $exportType);
            case 12:
                return $this->getTrumpalaikeNuomaXML($node, $exportType);
            case 13:
                return $this->getGarazasParduodaXML($node, $exportType);
            default:
                return '';
        }
    }

    private function getButasParduodaXML($node, $exportType)
    {
        $agent = node_load($node->field_broker_reference[$node->language][0]['nid']);

        if($exportType == self::TYPE_ALIO) {
            $parameters = explode(',', $node->title);
            $xml = '
          <skelbimas>
            <id>'.$node->nid.'</id>
            <category>'.$this->getTypeById($node->field_type[$node->language][0]['value']).'</category>
            <veiksmas>'.$this->getActionById($node->field_type[$node->language][0]['value']).'</veiksmas>
            <location_address>
                <parish>'.$this->getParish($parameters).'</parish>
                <city>'.$this->getCity($parameters).'</city>
                <street>'.$this->getStreet($parameters).'</street>
            </location_address>
            <busto_plotas_m>'.$node->field_area_total[$node->language][0]['value'].'</busto_plotas_m>
            <aukstu_sk_pastate>'.$node->field_num_floors[$node->language][0]['value'].'</aukstu_sk_pastate>
            <kitos_pastabos><![CDATA['.$node->body[$node->language][0]['value'].']]></kitos_pastabos>
            <kambariu_skaicius>'.$node->field_room_count[$node->language][0]['value'].'</kambariu_skaicius>
            <buto_aukstas>'.$node->field_this_floor[$node->language][0]['value'].'</buto_aukstas>
            <kaina>'.$node->field_price_eu[$node->language][0]['value'].'</kaina>
            <pardavejas>'.$agent->title.'</pardavejas>';
            $xml .= '<mobilus_telefonas>'.$agent->field_phone[$node->language][0]['value'].'</mobilus_telefonas>';
            if(!empty($agent->field_phone[$node->language][1]['value'])) {
                $xml .= '<telefonas_1>'.$agent->field_phone[$node->language][1]['value'].'</telefonas_1>';
            } else {
                $xml .= '<telefonas_1>'.$agent->field_phone[$node->language][0]['value'].'</telefonas_1>';
            }

            $xml .= '<el_pasto_adresas>'.$agent->field_email[$node->language][0]['value'].'</el_pasto_adresas>';
            if(!empty($node->field_object_image[$node->language])) {
                $xml .= '<nuotraukos>';
                foreach($node->field_object_image[$node->language] as $image) {
                    $xml .= '<photo>'.file_create_url($image['uri']).'</photo>';
                }
                $xml .= '</nuotraukos>';
            }

            $xml .= '</skelbimas>';
        } else {
            $xml = '';
        }

        return $xml;
    }

    private function getNamasParduodaXML($node, $exportType)
    {
        $agent = node_load($node->field_broker_reference[$node->language][0]['nid']);

        if($exportType == self::TYPE_ALIO) {
            $parameters = explode(',', $node->title);
            $xml = '
          <skelbimas>
            <id>'.$node->nid.'</id>
            <category>'.$this->getTypeById($node->field_type[$node->language][0]['value']).'</category>
            <veiksmas>'.$this->getActionById($node->field_type[$node->language][0]['value']).'</veiksmas>
            <location_address>
                <parish>'.$this->getParish($parameters).'</parish>
                <city>'.$this->getCity($parameters).'</city>
                <street>'.$this->getStreet($parameters).'</street>
            </location_address>
            <bendras_plotas_m>'.$node->field_area_total[$node->language][0]['value'].'</bendras_plotas_m>';

            if(isset($node->field_num_floors[$node->language][0]['value'])) {
                $xml .= '<aukstu_skaicius>'.$node->field_num_floors[$node->language][0]['value'].'</aukstu_skaicius>';
            }

            $xml .= '<kitos_pastabos><![CDATA['.$node->body[$node->language][0]['value'].']]></kitos_pastabos>
            <kambariu_skaicius>'.$node->field_room_count[$node->language][0]['value'].'</kambariu_skaicius>
            <kaina>'.$node->field_price[$node->language][0]['value'].'</kaina>
            <pardavejas>'.$agent->title.'</pardavejas>';
            $xml .= '<mobilus_telefonas>'.$agent->field_phone[$node->language][0]['value'].'</mobilus_telefonas>';
            if(!empty($agent->field_phone[$node->language][1]['value'])) {
                $xml .= '<telefonas_1>'.$agent->field_phone[$node->language][1]['value'].'</telefonas_1>';
            } else {
                $xml .= '<telefonas_1>'.$agent->field_phone[$node->language][0]['value'].'</telefonas_1>';
            }

            $xml .= '<el_pasto_adresas>'.$agent->field_email[$node->language][0]['value'].'</el_pasto_adresas>';
            if(!empty($node->field_object_image[$node->language])) {
                $xml .= '<nuotraukos>';
                foreach($node->field_object_image[$node->language] as $image) {
                    $xml .= '<photo>'.file_create_url($image['uri']).'</photo>';
                }
                $xml .= '</nuotraukos>';
            }

            $xml .= '</skelbimas>';
        } else {
            $xml = '';
        }

        return $xml;
    }

    private function getPatalposParduodaXML($node, $exportType)
    {
        $agent = node_load($node->field_broker_reference[$node->language][0]['nid']);

        if($exportType == self::TYPE_ALIO) {
            $parameters = explode(',', $node->title);
            $xml = '
          <skelbimas>
            <id>'.$node->nid.'</id>
            <category>'.$this->getTypeById($node->field_type[$node->language][0]['value']).'</category>
            <veiksmas>'.$this->getActionById($node->field_type[$node->language][0]['value']).'</veiksmas>
            <location_address>
                <parish>'.$this->getParish($parameters).'</parish>
                <city>'.$this->getCity($parameters).'</city>
                <street>'.$this->getStreet($parameters).'</street>
            </location_address>
            <patalpu_plotas_m>'.$node->field_area_total[$node->language][0]['value'].'</patalpu_plotas_m>
            <patalpos_yra_aukste>'.$node->field_num_floors[$node->language][0]['value'].'</patalpos_yra_aukste>
            <kitos_pastabos><![CDATA['.$node->body[$node->language][0]['value'].']]></kitos_pastabos>
            <kaina>'.$node->field_price_eu[$node->language][0]['value'].'</kaina>
            <pardavejas>'.$agent->title.'</pardavejas>';
            $xml .= '<mobilus_telefonas>'.$agent->field_phone[$node->language][0]['value'].'</mobilus_telefonas>';
            if(!empty($agent->field_phone[$node->language][1]['value'])) {
                $xml .= '<telefonas_1>'.$agent->field_phone[$node->language][1]['value'].'</telefonas_1>';
            } else {
                $xml .= '<telefonas_1>'.$agent->field_phone[$node->language][0]['value'].'</telefonas_1>';
            }

            $xml .= '<el_pasto_adresas>'.$agent->field_email[$node->language][0]['value'].'</el_pasto_adresas>';
            if(!empty($node->field_object_image[$node->language])) {
                $xml .= '<nuotraukos>';
                foreach($node->field_object_image[$node->language] as $image) {
                    $xml .= '<photo>'.file_create_url($image['uri']).'</photo>';
                }
                $xml .= '</nuotraukos>';
            }

            $xml .= '</skelbimas>';
        } else {
            $xml = '';
        }

        return $xml;
    }

    private function getButasNuomojaXML($node, $exportType)
    {
        $agent = node_load($node->field_broker_reference[$node->language][0]['nid']);

        if($exportType == self::TYPE_ALIO) {
            $xml = $this->getButasParduodaXML($node, $exportType);
        } else {
            $xml = '';
        }

        return $xml;
    }

    private function getNamasNuomojaXML($node, $exportType)
    {
        $agent = node_load($node->field_broker_reference[$node->language][0]['nid']);

        if($exportType == self::TYPE_ALIO) {
            $xml = $this->getNamasParduodaXML($node, $exportType);
        } else {
            $xml = '';
        }

        return $xml;
    }

    private function getPatalposNuomojaXML($node, $exportType)
    {
        $agent = node_load($node->field_broker_reference[$node->language][0]['nid']);

        if($exportType == self::TYPE_ALIO) {
            $xml = $this->getPatalposParduodaXML($node, $exportType);
        } else {
            $xml = '';
        }

        return $xml;
    }

    private function getSklypaiParduodaXML($node, $exportType)
    {
        $agent = node_load($node->field_broker_reference[$node->language][0]['nid']);

        if($exportType == self::TYPE_ALIO) {
            $parameters = explode(',', $node->title);
            $xml = '
          <skelbimas>
            <id>'.$node->nid.'</id>
            <category>'.$this->getTypeById($node->field_type[$node->language][0]['value']).'</category>
            <veiksmas>'.$this->getActionById($node->field_type[$node->language][0]['value']).'</veiksmas>
            <location_address>
                <parish>'.$this->getParish($parameters).'</parish>
                <city>'.$this->getCity($parameters).'</city>
                <street>'.$this->getStreet($parameters).'</street>
            </location_address>
            <sklypo_plotas_a>'.$node->field_area_total[$node->language][0]['value'].'</sklypo_plotas_a>
            <kitos_pastabos><![CDATA['.$node->body[$node->language][0]['value'].']]></kitos_pastabos>
            <kaina>'.$node->field_price_eu[$node->language][0]['value'].'</kaina>
            <pardavejas>'.$agent->title.'</pardavejas>';
            $xml .= '<mobilus_telefonas>'.$agent->field_phone[$node->language][0]['value'].'</mobilus_telefonas>';
            if(!empty($agent->field_phone[$node->language][1]['value'])) {
                $xml .= '<telefonas_1>'.$agent->field_phone[$node->language][1]['value'].'</telefonas_1>';
            } else {
                $xml .= '<telefonas_1>'.$agent->field_phone[$node->language][0]['value'].'</telefonas_1>';
            }

            $xml .= '<el_pasto_adresas>'.$agent->field_email[$node->language][0]['value'].'</el_pasto_adresas>';
            if(!empty($node->field_object_image[$node->language])) {
                $xml .= '<nuotraukos>';
                foreach($node->field_object_image[$node->language] as $image) {
                    $xml .= '<photo>'.file_create_url($image['uri']).'</photo>';
                }
                $xml .= '</nuotraukos>';
            }

            $xml .= '</skelbimas>';
        } else {
            $xml = '';
        }

        return $xml;
    }

    private function getSodybosParduodaXML($node, $exportType)
    {
        $agent = node_load($node->field_broker_reference[$node->language][0]['nid']);

        if($exportType == self::TYPE_ALIO) {
            $xml = $this->getNamasParduodaXML($node, $exportType);
        } else {
            $xml = '';
        }

        return $xml;
    }

    private function getTrumpalaikeNuomaXML($node, $exportType)
    {
        $agent = node_load($node->field_broker_reference[$node->language][0]['nid']);

        if($exportType == self::TYPE_ALIO) {
            $xml = $this->getButasParduodaXML($node, $exportType);
        } else {
            $xml = '';
        }

        return $xml;
    }

    private function getGarazasParduodaXML($node, $exportType)
    {
        $agent = node_load($node->field_broker_reference[$node->language][0]['nid']);

        if($exportType == self::TYPE_ALIO) {
            $parameters = explode(',', $node->title);
            $xml = '
          <skelbimas>
            <id>'.$node->nid.'</id>
            <category>'.$this->getTypeById($node->field_type[$node->language][0]['value']).'</category>
            <veiksmas>'.$this->getActionById($node->field_type[$node->language][0]['value']).'</veiksmas>
            <location_address>
                <parish>'.$this->getParish($parameters).'</parish>
                <city>'.$this->getCity($parameters).'</city>
                <street>'.$this->getStreet($parameters).'</street>
            </location_address>
            <garazo_plotas_m>'.$node->field_area_total[$node->language][0]['value'].'</garazo_plotas_m>
            <kitos_pastabos><![CDATA['.$node->body[$node->language][0]['value'].']]></kitos_pastabos>
            <kaina>'.$node->field_price_eu[$node->language][0]['value'].'</kaina>
            <pardavejas>'.$agent->title.'</pardavejas>';
            $xml .= '<mobilus_telefonas>'.$agent->field_phone[$node->language][0]['value'].'</mobilus_telefonas>';
            if(!empty($agent->field_phone[$node->language][1]['value'])) {
                $xml .= '<telefonas_1>'.$agent->field_phone[$node->language][1]['value'].'</telefonas_1>';
            } else {
                $xml .= '<telefonas_1>'.$agent->field_phone[$node->language][0]['value'].'</telefonas_1>';
            }

            $xml .= '<el_pasto_adresas>'.$agent->field_email[$node->language][0]['value'].'</el_pasto_adresas>';
            if(!empty($node->field_object_image[$node->language])) {
                $xml .= '<nuotraukos>';
                foreach($node->field_object_image[$node->language] as $image) {
                    $xml .= '<photo>'.file_create_url($image['uri']).'</photo>';
                }
                $xml .= '</nuotraukos>';
            }

            $xml .= '</skelbimas>';
        } else {
            $xml = '';
        }

        return $xml;
    }

    private function getParish($parameters) {
        return trim($parameters[0]);
    }

    private function getCity($parameters) {
        return trim($parameters[1]);
    }

    private function getStreet($parameters) {
        return !empty($parameters[2]) ? trim(substr($parameters[2], 0, strrpos($parameters[2], '.') + 1)) : '';
    }
}