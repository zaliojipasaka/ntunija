<?php

class Mapper {
    public $remove_str = array(
        'mstl.', 'k.', 'm.', 'kių', 'nių', 'iaus', 'lio',
        'nų', 'vos', 'dos', 'pio', 'ių II', 'kų II', 'nė',
        'nų I', 'čiai', 'čių', 'kų I', 'ių I', 'nų II',
        'dų', 'vos I', 'vos II', 'pių'
    );

    public function getAdminUnits() {
        $context  = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));
        $admUrl = 'http://domo.plius.lt/importhandler?datacollector=1&get_fk_placereg_adm_units=1';

        $xml = file_get_contents($admUrl, false, $context);
        $xml = simplexml_load_string($xml);
        $array = json_decode(json_encode((array)$xml), TRUE);
        $admUnits = array();

        foreach($array['response']['fk_placereg_adm_units_id']['item'] as $item) {
            $admUnits[$item['id']] = $item['title'];
        }

        return $admUnits;
    }

    public function getSettlements($adminUnit) {
        $context  = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));
        $admUrl = 'http://domo.plius.lt/importhandler?datacollector=1&fk_placereg_adm_units_id='.$adminUnit.'&get_fk_placereg_settlements_id=1';

        $xml = file_get_contents($admUrl, false, $context);
        $xml = simplexml_load_string($xml);
        $array = json_decode(json_encode((array)$xml), TRUE);
        $admUnits = array();

        foreach($array['response']['fk_placereg_settlements_id']['item'] as $item) {
            $admUnits[$item['id']] = $item['title'];
        }

        return $admUnits;
    }

    public function getMicroDistricts($adminUnit, $settlement) {
        $context  = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));
        $admUrl = 'http://domo.plius.lt/importhandler?datacollector=1&fk_placereg_adm_units_id='.$adminUnit.'&fk_placereg_settlements_id='.$settlement.'&get_fk_placereg_microdistricts_id=1';

        $xml = file_get_contents($admUrl, false, $context);
        $xml = simplexml_load_string($xml);
        $array = json_decode(json_encode((array)$xml), TRUE);
        $admUnits = array();

        foreach($array['response']['fk_placereg_microdistricts_id']['item'] as $item) {
            $admUnits[$item['id']] = $item['title'];
        }

        return $admUnits;
    }
}