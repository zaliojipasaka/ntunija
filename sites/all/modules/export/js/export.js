(function ($) {
    Drupal.behaviors.export = {
        attach: function (context, settings) {
            var views = $('.view-objektao-exportui', context);

            views.find('#alio-export-button').click(function (e) {
                $.ajax({
                    url: '/edomus/export/alio',
                    type: 'POST',
                    data: views.find('#export-form').serialize(),
                    success: function (data) {
                        alert(data);
                    },
                    dataType: 'xml'
                });
                e.stopPropagation();
                e.preventDefault();
            });
        }
    };
})(jQuery);
