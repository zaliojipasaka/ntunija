(function ($) {

Drupal.behaviors.initColorboxPlainStyle = {
  attach: function (context, settings) {
    $(context).bind('cbox_complete', function () {
      // Make all the controls invisible.
      $('#cboxCurrent, #cboxSlideshow', context).addClass('element-invisible');
      // Replace "Close" with "×" and show.
      $('#cboxClose', context).html('\327').addClass('cbox-close-plain');
      // Hide empty title.
      if ($('#cboxTitle:empty', context).length == true) {
        $('#cboxTitle', context).hide();
      }
      $('#cboxContent', context).on('mouseover', function () {
        $('#cboxClose', context).animate({opacity: 1}, {queue: false, duration: "fast"});
        if ($('#cboxTitle:empty', context).length == false) {
          $('#cboxTitle', context).slideDown();
        }
      });
      $('#cboxOverlay', context).on('mouseover', function () {
        $('#cboxClose', context).animate({opacity: 0}, {queue: false, duration: "fast"});
        if ($('#cboxTitle:empty', context).length == false) {
          $('#cboxTitle', context).slideUp();
        }
      });
      $('#cboxNext', context).hover(
          function() {
            $(this).addClass('on');
          }, function() {
            $(this).removeClass('on');
          }
      );
      $('#cboxPrevious', context).hover(
          function() {
            $(this).addClass('on');
          }, function() {
            $(this).removeClass('on');
          }
      );
    });
    $(context).bind('cbox_closed', function () {
      $('#cboxClose', context).removeClass('cbox-close-plain');
    });
  }
};

})(jQuery);
