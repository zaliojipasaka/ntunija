<?php

/**
 * @file
 * Individual slider item.
 *
 * Available variables:
 * - $image: Rendered image tag.
 * - $title: Title of the image.
 *
 * @see template_preprocess()
 * 
 * @ingroup themeable
 */
?>
<li><div><?php print $image; ?></div></li>
