(function ($) {
    Drupal.behaviors.edomus = {
        attach: function (context, settings) {
            var views  = $('.view-butai-pardavimui, .view-namai-pardavimui, .view-sklypai-pardavimui, .view-sodybos-sodai-pardavimui, .view-patalpos-pardavimui, .view-nuoma, .view-isimintini-objektai, .view-neskelbtini-objektai, .view-objektai', context);
            var agents = $('.view-agentai-blokas', context);
            views
                .find('.views-field-field-object-image, .views-field-field-price')
                .on('mouseover', function (event) {
                    $(this).parent().addClass('selected');
                })
                .on('mouseout', function(){
                    $(this).parent().removeClass('selected');
                });
            views
                .find('.views-field-field-object-image, .views-field-field-price')
                .hover(function() {
                    $(this).parent().find('.field-slideshow').cycle('resume');
                },function(){
                    $(this).parent().find('.field-slideshow').cycle('pause');
                });
            views
                .find('.views-field-field-photo')
                .on('mouseover', function (event) {
                    $(this).addClass('selected');
                })
                .on('mouseout', function(){
                    $(this).removeClass('selected');
                });
            views.find('.view-field-visit').click(function(e) {
                e.stopPropagation();
                e.preventDefault();
            });
            views.find('.view-field-contact').click(function(e) {
                e.stopPropagation();
                e.preventDefault();
            });
            agents.find('.view-field-email').each(function(){
                if ($(this).html().length > 20) {
                    $(this).html($(this).html().substr(0, 20) + '...');
                }
            });
            views.find('#object-adv-form').submit(function(e){
                $.ajax({
                    url: '/edomus/send/request',
                    type: 'POST',
                    data: views.find('#object-adv-form').serialize(),
                    success: function(data){
                        var message = 'Klaida siunčiant užklausą, pabandykite vėliau.';
                        if(data['sent']) {
                            message = 'Užklausa sėkmingai išsiųsta. Greitu laiku su Jumis susisieks mūsų darbuotojas.';
                        }
                        $.colorbox({
                            html: $('<h2>'+message+'</h2>'),
                            onComplete: function() {
                                setTimeout(function(){
                                    $().colorbox.close();
                                    views.find('#object-adv-form')[0].reset();
                                }, 3000);
                            }
                        });
                    },
                    dataType: 'json'
                });
                e.stopPropagation();
                e.preventDefault();
            });

            var description = $('#block-views-object-description-block', context);
            if(description.length > 0) {
                var elem = $("p", description).first();
                var textParts = elem.html().split("<br>");
                var first = "<h1 class='title'>" + textParts.shift() + ".</h1>";
                elem.html(first + textParts.join("<br>"));
            }

            views.find('.alio-export-button').click(function(e){
                $.ajax({
                    url: '/edomus/export/alio',
                    type: 'POST',
                    data: views.find('#export-form').serialize(),
                    success: function(data){
                        alert(data);
                    },
                    dataType: 'xml'
                });
                e.stopPropagation();
                e.preventDefault();
            });
        }
    };

    var views = $('.view-objektao-exportui');

    if(views.length > 0) {
        views
            .find('.views-field-field-object-image, .views-field-field-price')
            .hover(function() {
                $(this).parent().find('.field-slideshow').cycle('resume');
            },function(){
                $(this).parent().find('.field-slideshow').cycle('pause');
            });

        views.find('.alio-export-button').click(function(e){
            $.ajax({
                url: '/edomus/export/alio',
                type: 'POST',
                data: views.find('#export-form').serialize(),
                success: function(data){
                    alert(data);
                },
                dataType: 'xml'
            });
            e.stopPropagation();
            e.preventDefault();
        });
    }

})(jQuery);
