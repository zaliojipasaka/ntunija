<?php

/**
 * Class Utils used for storing commonly used utility functions
 */
class Utils
{
    /**
     * Converts PostgreSQL coordinate to Regio-style coordinate
     *
     * @param $pg
     *   PostgreSQL coordinate
     *
     * @return string
     *   Regio-style coordinate
     */
    public function ConvertToRegioCoordinate($pg)
    {
        return strtr($pg, array('(' => '', ')' => ''));
    }

    /**
     * Converts UTF8 string to HTML string
     */
    public function UTF82HTML($str = '')
    {
        if (function_exists('mb_convert_encoding')) {
            return mb_convert_encoding($str, 'HTML-ENTITIES', 'UTF-8');
        }
    }
}