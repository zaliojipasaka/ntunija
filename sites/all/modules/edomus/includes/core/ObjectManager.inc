<?php

/**
 * @file
 * Defines object manager type.
 */
module_load_include('inc', 'edomus', 'includes/core/Search');

/**
 * Class ObjectSaver
 */
class ObjectManager {
    /**
     * Aruodas api request types
     */
    const REQUEST_TYPE_ALL = 'getAllAdverts';
    const REQUEST_TYPE_ONE = 'getOneAdvert';

    /**
     * Returned object count from Aruodas api
     */
    const OBJECT_COUNT = 5000;

    /**
     * Aruodas api module object
     *
     * @var Aruodas_Module $objectProvider
     */
    protected $objectProvider = null;

    /**
     * Nodes processed by script
     *
     * @var array $processedNodeIds
     */
    protected $processedNodeIds = array();

    /**
     * Objects loaded from Aruodas api
     *
     * @var array $objects
     */
    protected $objects = array();

    /**
     * Prepares object manager for database sync with Aruodas api
     */
    public function prepare() {
        ini_set('max_execution_time', 600);
        $this->objectProvider = new Aruodas_Module();
        $this->_loadObjects();
    }

    /**
     * Loads all object nodes from database
     *
     * @var string $object_id
     *
     * @return mixed
     */
    private function _loadNode($object_id) {
        $query = new EntityFieldQuery();
        $query->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', 'objektas')
            ->fieldCondition('field_objekto_id', 'value', $object_id, 'LIKE');
        $result = $query->execute();

        if (!empty($result['node'])) {
            $node = array_pop($result['node']);
            return node_load($node->nid);
        }

        return null;
    }

    /**
     * Loads all objects from Aruodas api
     */
    private function _loadObjects() {
        foreach($this->objectProvider->getObjectTypes() as $objectType => $typeName) {
            $params = array
            (
                'AdvertLimit' => self::OBJECT_COUNT,
                'FType'       => $objectType
            );
            $objects = $this->objectProvider->processRequest(self::REQUEST_TYPE_ALL, $params);
            $this->objects[$objectType] = array_keys($objects[0]);
        }
    }

    /**
     * Cleans all used objects
     */
    public function clean() {
        $query = new EntityFieldQuery();
        $query->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', 'objektas')
            ->fieldCondition('field_objekto_id', 'value', null, 'is not')
            ->fieldCondition('field_objekto_id', 'value', 0, '!=')
            ->entityCondition('entity_id', $this->processedNodeIds, 'NOT IN');

        $result = $query->execute();

        if (!empty($result['node'])) {
            $nodeIds = array_keys($result['node']);
            node_delete_multiple($nodeIds);
        }

        unset($this->objects);
        unset($this->objectProvider);
        unset($this->processedNodeIds);
        unset($result);
    }

    /**
     * Syncs all objects loaded from Aruodas api to database nodes
     */
    public function syncObjectsWithDatabase() {
        foreach($this->objects as $objectType => $objectIds) {
            foreach($objectIds as $id) {
                $this->syncObjectWithLoadedNode($id, $objectType);
            }
        }
    }

    /**
     * Syncs object to node
     *
     * @param $id
     *   Object id
     * @param $objectType
     *   Object type
     */
    private function syncObjectWithLoadedNode($id, $objectType) {
        $params = array
        (
            'AdvertLimit' => self::OBJECT_COUNT,
            'FType'       => $objectType,
            'FObject'     => $id
        );

        $object = $this->objectProvider->processRequest(self::REQUEST_TYPE_ONE, $params);
        $object['type'] = $objectType;

        if($this->isObjectValid($object)) {
            $node = $this->_loadNode($object['uniqueId']);

            if (!empty($node)) {
                if($node->changed < strtotime($object['editDate'])) {
                    $this->_fill_and_save_node($node, $object);
                }
            } else {
                $node = new stdClass();
                $node->type = 'objektas';
                node_object_prepare($node);
                $this->_fill_and_save_node($node, $object);
            }

            $this->processedNodeIds[] = $node->nid;
            unset($node);
        }

        unset($object);
    }

    private function isObjectValid($object) {
        return !empty($object) && strtotime($object['expireDate']) > time() && $object['isActive'] == 1 && $object['sold'] == 0;
    }

    /**
     * Fills object node with object data and saves it
     *
     * @param $node stdClass
     *   Node object
     *
     * @param $object array
     *   Object data array
     */
    private function _fill_and_save_node(&$node, $object) {
        $node->title    = trim($object['title']);
        $node->language = LANGUAGE_NONE;
        $node->status   = 1;
        $node->promote  = 0;
        $node->sticky   = 0;
        $node->comment  = 0;
        $node->date     = trim($object['addDate']);
        $node->changed  = trim($object['editDate']);
        $node->uid = 1;
        $object['description'] = trim(nl2br($object['description']));
        $descParts = explode('<br />', $object['description']);
        $this->fillField($node->body[$node->language], trim($object['description']));
        $node->body[$node->language][0]['summary'] = trim($descParts[0]);
        $node->body[$node->language][0]['format']  = 'filtered_html';
        $price = str_ireplace(' ', '', substr($object['price'], 0, strrpos(trim($object['price']), ' ')));
        $priceEu = str_ireplace(' ', '', substr($object['priceEur'], 0, strrpos(trim($object['priceEur']), ' ')));
        $priceM2 = str_ireplace(' ', '', substr($object['pricePer'], 0, strrpos(trim($object['pricePer']), ' ')));
        $priceM2Eu = str_ireplace(' ', '', substr($object['pricePer'], 0, strrpos(trim($object['pricePer']), ' ')));
        $area = str_ireplace(' ', '', substr($object['fields']['AreaOverall'], 0, strrpos(trim($object['fields']['AreaOverall']), ' ')));
        $areaTotal = str_ireplace(' ', '', substr($object['pricePerEur'], 0, strrpos(trim($object['pricePerEur']), ' ')));
        // Node fields
        $this->fillField($node->field_objekto_id[$node->language], $this->sanitizeField($object['uniqueId']));
        if(isset($object['fields']['RoomNum'])) {
            $this->fillField($node->field_room_count[$node->language], $this->sanitizeField($object['fields']['RoomNum']));
        }
        if(isset($object['fields']['Floor'])) {
            $this->fillField($node->field_this_floor[$node->language], $this->sanitizeField($object['fields']['Floor']));
        }
        if(isset($object['fields']['HouseHeight'])) {
            $this->fillField($node->field_num_floors[$node->language], $this->sanitizeField($object['fields']['HouseHeight']));
        }
        $priceArray = explode('-', str_ireplace(',', '.',$price));
        $price = $priceArray[0];
        $priceEuArray = explode('-', str_ireplace(',', '.',$priceEu));
        $priceEu = $priceEuArray[0];
        $priceM2Array = explode('-', str_ireplace(',', '.',$priceM2));
        $priceM2 = $priceM2Array[0];
        $priceM2EuArray = explode('-', str_ireplace(',', '.',$priceM2Eu));
        $priceM2Eu = $priceM2EuArray[0];
        $areaArray = explode('-', str_ireplace(',', '.',$area));
        $area = $areaArray[0];
        $areaTotalArray = explode('-', str_ireplace(',', '.',$areaTotal));
        $areaTotal = $areaTotalArray[0];
        $this->fillField($node->field_area_total[$node->language], $this->sanitizeField($area));
        var_dump($node->field_area_total[$node->language]);
        $this->fillField($node->field_area_ground[$node->language], $this->sanitizeField($areaTotal));
        $this->fillField($node->field_field_price_m2_eu[$node->language], $priceM2Eu);
        $this->fillField($node->field_price[$node->language], $price);
        $this->fillField($node->field_price_m2[$node->language], $priceM2);
        $this->fillField($node->field_type[$node->language], $object['type']);
        $this->fillField($node->field_expires[$node->language], trim($object['expireDate']));
        $this->fillField($node->field_adresas[$node->language], trim($object['title']));
        $this->fillField($node->field_price_eu[$node->language], $priceEu);

        // Images
        if(!empty($object['images'])) {
            foreach($object['images'] as $i => $image) {
                $imageFile = file_get_contents($image['big']);
                $ext = substr($image['big'], -3, 3);

                $file = file_save_data($imageFile, 'public://edomus/objects/'.trim($object['uniqueId']).'_'.$i.'.'.$ext, FILE_EXISTS_REPLACE);

                $node->field_object_image[$node->language][]['fid'] = $file->fid;
            }
        }
        // Node Agentas reference
        if(!empty($object['emailFull'])) {
            $agentId = $this->_get_nid_by_email(trim($object['emailFull']));
            if($agentId === null) {
                $agent = new stdClass();
                $agent->type = 'agentas';
                node_object_prepare($agent);

                $agentDetails = array(
                    'email' => trim($object['emailFull']),
                    'phones' => array(
                        $object['phone1'],
                        $object['phone2']
                    )
                );

                $this->_fill_and_save_agent($agent, $agentDetails);
                $agentId = $agent->nid;
            }

            $node->field_broker_reference[$node->language][]['nid'] = $agentId;
        }

        $node = node_submit($node);

        if($node) {
            node_save($node);
        }
    }

    /**
     * Fills agent node with agent data and saves it
     *
     * @param $node stdClass
     *   Node agent
     * @param $agent array
     *   Agent data array
     */
    private function _fill_and_save_agent(&$node, $agent) {
        // Node properties
        $node->title    = trim($agent['email']);
        $node->language = LANGUAGE_NONE;
        $node->status   = 1;
        $node->promote  = 0;
        $node->sticky   = 0;
        $node->comment  = 0;
        $node->uid = 1;

        // Node fields
        $node->field_email[$node->language][0]['value'] = trim(strtolower($agent['email']));
        $agent['phones'];

        foreach($agent['phones'] as $phone) {
            $node->field_phone[$node->language][]['value'] = trim($phone);
        }

        $node = node_submit($node);

        if($node) {
            node_save($node);
        }
    }

    /**
     * Get agent node id by email
     *
     * @param $email int
     *   Email of agent to find
     *
     * @return null|int Node id or null if node not found
     */
    private function _get_nid_by_email($email) {
        $query = new EntityFieldQuery;
        $nid   = null;
        $entities = $query->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', 'agentas')
            ->fieldCondition('field_email', 'value', strtolower($email), '=')
            ->execute();
        if(isset($entities) && is_array($entities) && count($entities) > 0) {
            $nodes = array_pop($entities);
            $nid = array_pop($nodes)->nid;
        }

        return $nid;
    }

    private function fillField(&$field, $value) {
        if(!empty($value) && $value != '') {
            $field[0]['value'] = $value;
        }
    }

    private function sanitizeField($field) {
        $values = explode(' ', trim($field));

        if(!empty($values[0])) {
            return $values[0];
        }

        return null;
    }
} 