<?php

class Aruodas_Module
{

    private $requestPath = 'http://www.aruodas.lt/api/';
    private $requestToken = '7f596812e6fca73a512a4712605c925a';

    public function getSearchColumns($objectType)
    {
        $columns = array();
        switch ($objectType) {
            case '1' :
            case '4' :
                $columns['rooms'] = 'Kambarių sk.';
                $columns['area'] = 'Plotas (m²)';
                $columns['floor'] = 'Aukštai';
                break;
            case '2':
            case '5':
                $columns['area'] = 'Plotas (m²)';
                $columns['AreaLot'] = 'Plotas (a)';
                $columns['HouseStates'] = 'Įrengimas';
                break;
            case '3':
            case '6':
                $columns['area'] = 'Plotas (m²)';
                $columns['floor'] = 'Aukštai';
                $columns['Intendances'] = 'Paskirtis';
                break;
            case '9':
                $columns['AreaLot'] = 'Plotas (a)';
                $columns['area'] = 'Plotas (m²)';
                $columns['WaterAccumDist'] = 'Iki vandens';
                break;
            case '10':
                $columns['BuyType'] = 'Objekto tipas';
                break;
            case '11':
                $columns['area'] = 'Plotas (a)';
                $columns['Intendances'] = 'Paskirtis';
                break;
            case '12':
                $columns['rooms'] = 'Kambarių sk.';
                $columns['area'] = 'Plotas (m²)';
                break;
            case '13':
                $columns['area'] = 'Plotas (m²)';
                break;
        }
        return $columns;
    }

    public function getColumnTranslation()
    {
        return array(
            'Price' => 'Kaina',
            'Special' => 'Ypatybės',
            'HouseNum' => 'Namo numeris',
            'ApartNum' => 'Buto numeris',
            'RoomNum' => 'Kambarių sk.',
            'HouseType' => 'Namo tipas',
            'HouseState' => 'Įrengimas',
            'HouseHeight' => 'Aukštų sk.',
            'Floor' => 'Aukštas',
            'AreaLot' => 'Plotas (a)',
            'AreaOverall' => 'Plotas (m²)',
            'AreaKitchen' => 'Virtuvės plotas',
            'WarmSystem' => 'Šildymas',
            'WaterSystem' => 'Vanduo',
            'Intendance' => 'Įrengimas',
            'BuildYear' => 'Metai',
            'ObjectState' => 'Būklė',
            'CityDist' => 'Atstumas iki miesto centro (km)',
            'WaterAccumName' => 'Artimiausias vandens telkinys',
            'WaterAccumDist' => 'Iki vandens telkinio',
            'PremSum' => ' ',
            'RentType' => 'Nuomos tipas',
            'GarageState' => 'Būklė',
            'GarageType' => 'Garažo tipas',
            'GarageCarsFit' => 'Telpa automobilių',
            'GarageFeatures' => 'Garažo savybės',
            'OfferType' => 'Tipas',
        );
    }

    public function getSeoRegion($region) {
        $seoNames = array(
            '0' => '',
            '461' => 'Vilniuje',
            '43' => 'Kaune',
            '112' => 'Klaipėdoje',
            '259' => 'Šiauliuose',
            '205' => 'Panevėžyje',
            '2' => 'Alytuje',
            '114' => 'Palangoje',

            '260' => 'Akmenėje',
            '4' => 'Alytaus rajone',
            '393' => 'Anykščiuose',
            '42' => 'Birštone',
            '206' => 'Biržuose',
            '3' => 'Druskininkuose',
            '1191' => 'Elektrėnuose',
            '413' => 'Ignalinoje',
            '44' => 'Jonavoje',
            '267' => 'Joniškyje',
            '346' => 'Jurbarke',
            '53' => 'Kaišiadoryse',
            '1192' => 'Kalvarijoje',
            '63' => 'Kauno rajone',
            '1193' => 'Kazlų Rūdoje',
            '215' => 'Kėdainiuose',
            '278' => 'Kelmėje',
            '115' => 'Klaipėdos rajone',
            '127' => 'Kretingoje',
            '228' => 'Kupiškyje',
            '27' => 'Lazdijuose',
            '176' => 'Marijampolės rajone',
            '162' => 'Marijampolėje',
            '359' => 'Mažeikiuose',
            '426' => 'Molėtuose',
            '113' => 'Neringoje',
            '1196' => 'Pagėgiuose',
            '289' => 'Pakruojyje',
            '235' => 'Panevėžio rajone',
            '247' => 'Pasvalyje',
            '367' => 'Plungėje',
            '86' => 'Prienuose',
            '298' => 'Radviliškyje',
            '98' => 'Raseiniuose',
            '1197' => 'Rietave',
            '438' => 'Rokiškyje',
            '136' => 'Skuode',
            '187' => 'Šakiuose',
            '513' => 'Šalčininkuose',
            '311' => 'Šiaulių rajone',
            '332' => 'Šilalėje',
            '146' => 'Šilutėje',
            '539' => 'Širvintose',
            '526' => 'Švenčionyse',
            '323' => 'Tauragėje',
            '381' => 'Telšiuose',
            '487' => 'Trakuose',
            '501' => 'Ukmergėje',
            '449' => 'Utenoje',
            '16' => 'Varėnoje',
            '163' => 'Vilkaviškyje',
            '462' => 'Vilniaus rajone',
            '459' => 'Visagine',
            '403' => 'Zarasuose',
            '99999' => 'Visoje Lietuvoje',
        );

        return $seoNames[$region];
    }

    public function getObjectTypes()
    {
        return array(
            1 => 'Butai pardavimui',
            2 => 'Namai pardavimui',
            3 => 'Patalpos pardavimui',
            4 => 'Butai nuomai',
            5 => 'Namai nuomai',
            6 => 'Patalpos nuomai',
            11 => 'Sklypai',
            13 => 'Garažai',
            9 => 'Sodybos, sodai',
            12 => 'Trumpalaikė nuoma',
        );
    }

    public function processRequest($requestMethod, $requestParams = array())
    {
        $requestOptions = array(
            'http' => array(
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'method' => 'POST',
                'user_agent' => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
                'content' => http_build_query(
                    array(
                        'token' => $this->requestToken,
                        'method' => $requestMethod,
                        'variables' => $requestParams
                    )
                ),
            ),
        );

        $requestStream = stream_context_create($requestOptions);
        $apiResult = file_get_contents($this->requestPath, false, $requestStream);

        return json_decode($apiResult, true);
    }
}
	