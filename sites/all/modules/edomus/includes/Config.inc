<?php
/**
 * @file
 * File holding module configuration got from http://edomus.lt.
 */

DEFINE('FORM_SHOW_ADVANCED',FALSE);
DEFINE('KV_MAIN_URL','http://www.edomus.lt/');
DEFINE('CLIENT_COMPANY',11574);
DEFINE('MASTER_IS_ACTIVE',TRUE);
DEFINE('COUNTRY',3);
DEFINE('COUNTRY_PREF','lt');
