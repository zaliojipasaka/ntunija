<?php

class Master {

    /**
     * @var bool
     */
    var $online = true;

    /**
     * @var string
     */
    var $baseURL;

    /**
     * @var array
     */
    var $items;

    /**
     * @var int
     */
    var $objects_count;

    /**
     * @var FileUtils
     */
    var $fileUtils;

    /**
     * @var XML
     */
    var $xmlPreprocessor;

    /**
     * Constructor
     *
     * @param FileUtils $fileUtils
     * @param XML $xmlPreprocessor
     */
    function __construct($fileUtils, $xmlPreprocessor){
		$this->items = array();
		$this->baseURL = KV_SEARCH_URL.'&';
		$this->objects_count = 0;
        $this->fileUtils = $fileUtils;
        $this->xmlPreprocessor = $xmlPreprocessor;
	}

    /**
     * Searches for objects
     *
     * @param array $params
     *   Object search parameters
     *
     * @return array
     *   Objects found by search
     */
    public function search( $params=array() ) {
		$querystring = '';
		foreach ($params as $kw=>$value) {
			if (is_array($value)) {
				foreach ($value as $k=>$v) {
					$querystring .= '&'.urlencode($kw.'['.$k.']').'='.urlencode($v);
				}
			} else {
				$querystring .= '&'.urlencode($kw).'='.urlencode($value);
			}
		}

		$kv_search_result	= $this->xmlPreprocessor->getXMLTree($this->fileUtils->getFile($this->baseURL.$querystring.'&company_id='.CLIENT_COMPANY.'&withlimit=1'));
		$this->setObjectsCount( $kv_search_result['SEARCHRESULT'][0]['OBJECTCOUNT'][0]['VALUE'] );
		return $kv_search_result;
	}

    /**
     * Returns objects count
     *
     * @return int
     *   Objects count
     */
    public function getObjectsCount(){
		return $this->objects_count;
	}

    /**
     * Sets objects count
     *
     * @param int $count
     *   Objects count
     */
    public function setObjectsCount($count){
		$this->objects_count = $count;
	}

    /**
     * Returns master status
     *
     * @return bool
     *   Master status
     */
    public function getMasterStatus(){
        return $this->online;
    }

    /**
     * Sets master status
     *
     * @param bool $status
     *   Master status
     */
    public function setMasterStatus($status){
        $this->online = $status;
    }
}