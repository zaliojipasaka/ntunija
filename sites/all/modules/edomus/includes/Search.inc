<?php
/**
 *    Main class for searching engine.
 *    It contains methods for printing out search form, for setting some options
 *    for printing out search result/object.
 *    This class needs some additional files which had to be installed with this class.
 */

/**
 * @includes
 * External files required by search engine.
 */
module_load_include('inc', 'edomus', 'includes/Config');
module_load_include('inc', 'edomus', 'includes/Utils');
module_load_include('inc', 'edomus', 'includes/FileUtils');
module_load_include('inc', 'edomus', 'includes/XML');
module_load_include('inc', 'edomus', 'includes/Master');

/**
 * Defines main search engine URLs
 */
if (!defined('KV_SEARCH_URL'))
    DEFINE('KV_SEARCH_URL', KV_MAIN_URL . '?act=search.xml');

if (!defined('KV_OBJECTS_IMPORT_URL'))
    DEFINE('KV_OBJECTS_IMPORT_URL', KV_MAIN_URL . '?act=object.xml&object_ids=');

/**
 * Class Search is the main class for search engine
 */
class Search
{
    var $master;
    var $result = array();
    var $counties;
    var $cities;
    var $parish;
    var $lang;
    var $found = 0;
    var $timer;
    var $debug = array();
    var $valid_att;
    var $att;
    var $siteatt;
    var $links;
    var $fileUtils;
    var $xmlPreprocessor;
    var $utils;

    function __construct($lang = 'lt', $params = array())
    {
        $this->utils = new Utils();
        $this->fileUtils = new FileUtils();
        $this->xmlPreprocessor = new XML();
        $this->master = new Master($this->fileUtils, $this->xmlPreprocessor);
        $this->lang = $lang;
        $this->links = array();
    }

    /**
     * Set search option if it is available
     */
    function setSearchOption($name = '', $value = '')
    {
        if (isset($this->valid_att[$name])) {
            if ($this->att['building_type'] == 66)
                $this->att['building_type'] = null;
            if ($this->att['building_type'] == 14)
                $this->att['building_type'] = null;
            $this->att[$name] = $value;
            $_GET[$name] = $value;
        }
    }

    /**
     * Set search options
     */
    function setSearchOptions($options = array())
    {
        if (!is_array($options)) {
            return false;
        }
        foreach ($options AS $option => $val) {
            $this->setSearchOption($option, $val);
        }
        return true;
    }

    /**
     * Method for object searching.
     */
    function searchObjects()
    {
        if ($this->master->getMasterStatus()) {
            $m_search_res = $this->master->search($this->att);
            $this->found = $this->master->getObjectsCount();
            $this->setSearchOption('search_id', (int)$m_search_res['SEARCHRESULT'][0]['RESULTID'][0]['VALUE']);
            $m_search_res = $m_search_res['SEARCHRESULT'][0]['OBJECTS'][0]['OBJECT'];
            return $m_search_res;
        } else {
            return false;
        }
    }

    /**
     * Method for searching object data by object ids
     */
    function searchObjectsData($m_search_res)
    {
        if ($this->master->getMasterStatus() && $m_search_res !== false) {
            $m_search_res = $this->XMLSA2ValidSA($m_search_res);
            if (count($m_search_res)) {
                $m_import = $this->xmlPreprocessor->getXMLTree($this->fileUtils->getFile(KV_OBJECTS_IMPORT_URL . join(',', $m_search_res)));
                foreach ($m_import['OBJECTS'][0]['OBJECT'] as $object) {
                    $m_search_res[$object['OBJECTID'][0]['VALUE']] = $this->XMLOA2ValidOA($object);
                }
                $this->result = $m_search_res;
            }
            unset($m_search_res);
            return $this->result;
        } else {
            return false;
        }
    }

    /**
     * Transfers XML structured object array in to PHP structured object array
     *
     * @param array $object
     *   Object XML data for structuring
     *
     * @return array
     *   PHP structured array
     */
    function XMLOA2ValidOA($object = array())
    {
        $prepare = array(
            'object_id' => isset($object['OBJECTID'][0]['VALUE']) ? $object['OBJECTID'][0]['VALUE'] : null,
            'type' => isset($object['TYPE'][0]['VALUE']) ? $object['TYPE'][0]['VALUE'] : null,
            'building_type' => isset($object['BUILDING_TYPE'][0]['VALUE']) ? $object['BUILDING_TYPE'][0]['VALUE'] : null,
            'deal' => isset($object['DEAL'][0]['VALUE']) ? $object['DEAL'][0]['VALUE'] : null,
            'price' => isset($object['PRICE'][0]['VALUE']) ? $object['PRICE'][0]['VALUE'] : null,
            'new_price' => isset($object['NEW_PRICE'][0]['VALUE']) ? $object['NEW_PRICE'][0]['VALUE'] : null,
            'currency' => isset($object['PRICE'][0]['ATTRIBUTES']['CURRENCY']) ? $object['PRICE'][0]['ATTRIBUTES']['CURRENCY'] : null,
            'price_m2' => isset($object['PRICEPER'][0]['VALUE']) ? $object['PRICEPER'][0]['VALUE'] : null,
            'date_modified' => isset($object['MODIFIEDDATE'][0]['VALUE']) ? $object['MODIFIEDDATE'][0]['VALUE'] : null,
            'date_created' => isset($object['CREATEDDATE'][0]['VALUE']) ? $object['CREATEDDATE'][0]['VALUE'] : null,
            'date_expires' => isset($object['EXPIRESDATE'][0]['VALUE']) ? $object['EXPIRESDATE'][0]['VALUE'] : null,
            'num_floors' => isset($object['NUM_FLOORS'][0]['VALUE']) ? $object['NUM_FLOORS'][0]['VALUE'] : null,
            'this_floor' => isset($object['THIS_FLOOR'][0]['VALUE']) ? $object['THIS_FLOOR'][0]['VALUE'] : null,
            'room_count' => isset($object['ROOM_COUNT'][0]['VALUE']) ? $object['ROOM_COUNT'][0]['VALUE'] : null,
            'year_built' => isset($object['YEAR_BUILT'][0]['VALUE']) ? $object['YEAR_BUILT'][0]['VALUE'] : null,
            'area_total' => isset($object['AREA_TOTAL'][0]['VALUE']) ? $object['AREA_TOTAL'][0]['VALUE'] : null,
            'area_ground' => isset($object['AREA_GROUND'][0]['VALUE']) ? $object['AREA_GROUND'][0]['VALUE'] : null,
            'condition' => isset($object['CONDITION'][0]['VALUE']) ? $object['CONDITION'][0]['VALUE'] : null,
            'building_status' => isset($object['BUILDING_STATUS'][0]['VALUE']) ? $object['BUILDING_STATUS'][0]['VALUE'] : null,
            'broker' => array(
                'name' => isset($object['BROKER'][0]['NAME'][0]['VALUE']) ? $object['BROKER'][0]['NAME'][0]['VALUE'] : null,
                'email' => isset($object['BROKER'][0]['EMAIL'][0]['VALUE']) ? $object['BROKER'][0]['EMAIL'][0]['VALUE'] : null,
                'phone' => isset($object['BROKER'][0]['PHONE'][0]['VALUE']) ? $object['BROKER'][0]['PHONE'][0]['VALUE'] : null,
                'company' => isset($object['BROKER'][0]['COMPANY'][0]['VALUE']) ? $object['BROKER'][0]['COMPANY'][0]['VALUE'] : null,
                'photo' => isset($object['BROKER'][0]['PHOTO'][0]['VALUE']) ? $object['BROKER'][0]['PHOTO'][0]['VALUE'] : null
            ),
            'title' => array(),
            'info' => array(),
            'details' => array(),
            'images' => array()
        );
        if (is_array($object['TITLE'])) foreach ($object['TITLE'] as $title)
            $prepare['title'][$title['ATTRIBUTES']['LANGUAGE']] = $title['VALUE'];
        if (is_array($object['INFO'])) foreach ($object['INFO'] as $info)
            $prepare['info'][$info['ATTRIBUTES']['LANGUAGE']] = $info['VALUE'];
        if (is_array($object['DETAILS'])) foreach ($object['DETAILS'] as $detail)
            $prepare['details'][$detail['ATTRIBUTES']['LANGUAGE']] = $detail['VALUE'];
        if (isset($object['IMAGES'][0]['IMAGE']) && is_array($object['IMAGES'][0]['IMAGE']) && count($object['IMAGES'][0]['IMAGE'])) {
            foreach ($object['IMAGES'][0]['IMAGE'] as $i => $image) {
                $prepare['images'][$i]['thumbnailURI'] = $image['THUMBNAILURI'][0]['VALUE'];
                $prepare['images'][$i]['imageURI'] = $image['IMAGEURI'][0]['VALUE'];
                $prepare['images'][$i]['position'] = $image['POSITION'][0]['VALUE'];
            }
        }
        return $prepare;
    }

    /**
     * Reformats object ids array
     *
     * @param $search_array
     *   Array with object ids
     *
     * @return array|null
     *  Reformatted array with object ids
     */
    function XMLSA2ValidSA($search_array = array())
    {
        if (!is_array($search_array) OR !count($search_array))
            return null;
        $array = array();
        foreach ($search_array as $so) {
            $array[$so['OBJECTID'][0]['VALUE']] = $so['OBJECTID'][0]['VALUE'];
        }
        return $array;
    }

    /**
     * Returns several objects by given limit
     *
     * @param $limit
     *   Number of objects to return
     *
     * @return array|bool
     *   Data array of objects or false on error
     */
    function getObjects($limit)
    {
        $this->att['page_size'] = $limit;
        return $this->searchObjectsData($this->searchObjects($this->att));
    }

    /**
     * Returns object data by id
     *
     * @param $object_id
     *   Object ID
     *
     * @return array
     *   Object data
     */
    function getObject($object_id = 0)
    {
        $object_xml_array = $this->xmlPreprocessor->getXMLTree($this->fileUtils->getFile(KV_OBJECTS_IMPORT_URL . $object_id));
        $object = $this->XMLOA2ValidOA($object_xml_array['OBJECTS'][0]['OBJECT'][0]);
        return $object;
    }
}