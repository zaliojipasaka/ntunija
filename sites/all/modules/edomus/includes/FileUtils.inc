<?php

/**
 * Class FileUtils used for requesting objects in XML format from http://edomus.lt
 */
class FileUtils
{

    /**
     * @var string
     */
    var $url = '';

    /**
     * @var string
     */
    var $host = '';

    /**
     * @var string
     */
    var $file = '';

    /**
     * Constructor
     *
     * @param string $url
     *   URL for object XML retrieval
     * @param string $host
     *   User host
     */
    function __construct($url = '', $host = '')
    {
        $this->host = $host;
        $this->url = $url;
    }

    /**
     * Sets host
     *
     * @param string $host
     *   User host
     */
    public function setHost($host = '')
    {
        $this->host = $host;
    }

    /**
     * Gets host
     *
     * @return string
     *   User host
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Sets URL
     *
     * @param string $url
     *   URL for object XML retrieval
     */
    public function setURL($url = '')
    {
        $this->url = $url;
    }

    /**
     * Gets URL
     *
     * @return string
     *   URL for object XML retrieval
     */
    public function getURL()
    {
        return $this->url;
    }

    /**
     * Retrieves XML file with objects
     *
     * @param string $url
     *   URL for object XML retrieval
     *
     * @return int|null|string
     *   XML with objects
     */
    public function getFile($url = '')
    {
        if (strlen($url)) {
            $this->url = $url;
        }
        if ((int)ini_get('allow_url_fopen') AND $handler = fopen($this->url, 'r')) {
            return $this->_getViaFopen($handler);
        }
        if (!$this->host) {
            return -1;
        }
        if (!$this->url) {
            return -2;
        }
    }

    /**
     * Gets file contents from given handler
     *
     * @param $handler
     *   Handler got from fopen method
     *
     * @return null|string
     *   File contents
     */
    private function _getViaFopen($handler)
    {
        $data = '';
        if (!is_resource($handler)) {
            return null;
        }
        while (!feof($handler)) {
            $data .= fread($handler, 1024);
        }
        fclose($handler);
        return $data;
    }
}