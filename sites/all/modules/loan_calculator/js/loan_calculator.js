(function ($) {
    Drupal.behaviors.loan_calculator = {
        attach: function (context, settings) {
            var tableDiv = document.getElementById('payment-table');

            if(tableDiv) {
                var ownPercentage,
                    price,
                    sum,
                    period,
                    percent1,
                    percent2,
                    type1,
                    type2;

                function setValues() {
                    ownPercentage = parseInt(document.getElementById("own_percentage").value.trim()) || 15;
                    price = parseInt(document.getElementById('price').value.trim()) || 100000;
                    sum = parseFloat(document.getElementById('sum').value.trim()) || (price * (100 - ownPercentage) / 100);
                    period = (parseInt(document.getElementById('period').value.trim()) || 25) * 12;
                    percent1 = parseFloat(document.getElementById('percent_1').value.trim()) || 2;
                    percent2 = parseFloat(document.getElementById('percent_2').value.trim()) || 2.5;
                    type1 = document.getElementById('type_1').value.trim() || 1;
                    type2 = document.getElementById('type_2').value.trim() || 1;

                    document.getElementById('sum').value = sum;
                }

                function methodName(type) {
                    var types = {
                        '1': 'linij.',
                        '2': 'anuit.'
                    };

                    return types[type] || types['1'];
                }

                function calculateLinear(sum, period, percent) {
                    var i, data = [], interest, left,
                        credit = sum / period;

                    for (i = 0; i < period; i++) {
                        interest = sum * (percent / 100);
                        left = sum;
                        sum = sum - credit;
                        data.push({
                            left: left,
                            credit: credit,
                            interest: interest,
                            return: credit + interest
                        });
                    }

                    return data;
                }

                function calculateAnuity(sum, period, percent) {
                    percent = percent / 100;
                    var i,
                        data = [],
                        left,
                        k = Math.pow((1 + (percent / 12)), period),
                        koef = ((percent / 12) * k) / (k - 1),
                        interest,
                        returnValue = sum * koef;

                    for (i = 0; i < period; i++) {
                        left = sum;
                        interest = (sum * percent * (period / 12)) / period;
                        sum = sum - (returnValue - interest);
                        data.push({
                            left: left,
                            credit: returnValue - interest,
                            interest: interest,
                            return: returnValue
                        });
                    }

                    return data;
                }

                function calculateByType(type, sum, period, percent) {
                    switch (type) {
                        case '1':
                            return calculateLinear(sum, period, percent / 12);
                        case '2':
                            return calculateAnuity(sum, period, percent);
                    }
                }

                function calculateTableData(type1, type2) {
                    var data = [];

                    data.push(calculateByType(type1, sum, period, percent1));
                    data.push(calculateByType(type2, sum, period, percent2));

                    return data;
                }

                function renderTable() {
                    setValues();

                    var tableHTML = '<table>',
                        tableData = calculateTableData(type1, type2),
                        totals = calculateTotals(tableData);

                    tableHTML += renderHeader() + '<tbody>';

                    tableData[0].forEach(function (data1, index) {
                        var data2 = tableData[1][index];
                        tableHTML += renderRow(index, data1, data2);
                    });

                    tableHTML += '</tbody>' + renderFooter(totals) + '</table>';
                    tableDiv.innerHTML = tableHTML;

                    updateSummary(totals);
                }

                function updateSummary(totals) {
                    var returnValue = totals['interest1'] - totals['interest2'],
                        month = returnValue / period,
                        year = returnValue / (period / 12);

                    $('.percent_1').html(percent1);
                    $('.percent_2').html(percent2);
                    $('.pay_1').html(totals['interest1'].toFixed(2));
                    $('.pay_2').html(totals['interest2'].toFixed(2));
                    $('.method_1').html(methodName(type1));
                    $('.method_2').html(methodName(type2));
                    $('.total').addClass(colorClass(returnValue)).html(module(returnValue).toFixed(2));
                    $('.month').addClass(colorClass(month)).html(module(month).toFixed(2));
                    $('.year').addClass(colorClass(year)).html(module(year).toFixed(2));
                }

                function renderHeader() {
                    return '<thead><tr>' +
                        '<th>Mėn.</th>' +
                        '<th>Kredito likutis ' + percent1 + '%' + methodName(type1) + '</th>' +
                        '<th>Kredito likutis ' + percent2 + '%' + methodName(type2) + '</th>' +
                        '<th>Grąžinamas kreditas ' + percent1 + '%' + methodName(type1) + '</th>' +
                        '<th>Grąžinamas kreditas ' + percent2 + '%' + methodName(type2) + '</th>' +
                        '<th>Mėnesio palūkanos ' + percent1 + '%' + methodName(type1) + '</th>' +
                        '<th>Mėnesio palūkanos ' + percent2 + '%' + methodName(type2) + '</th>' +
                        '<th>Viso per mėnesį ' + percent1 + '%' + methodName(type1) + '</th>' +
                        '<th>Viso per mėnesį ' + percent2 + '%' + methodName(type2) + '</th>' +
                        '<th>Skirtumas</th>' +
                        '</tr></thead>';
                }

                function calculateTotals(tableData) {
                    var totals = {
                        left1: tableData[0][0]['left'],
                        left2: tableData[1][0]['left'],
                        credit1: 0,
                        credit2: 0,
                        interest1: 0,
                        interest2: 0,
                        return1: 0,
                        return2: 0
                    };
                    tableData[0].forEach(function (data1, index) {
                        var data2 = tableData[1][index];
                        totals['credit1'] += data1['credit'];
                        totals['credit2'] += data2['credit'];
                        totals['interest1'] += data1['interest'];
                        totals['interest2'] += data2['interest'];
                        totals['return1'] += data1['return'];
                        totals['return2'] += data2['return'];
                    });

                    return totals;
                }

                function renderFooter(totals) {
                    var returnValue = totals['return2'] - totals['return1'];
                    return '<tfoot><tr>' +
                        '<td><strong>Viso:</strong></td>' +
                        '<td class="blue-text"><strong>' + totals['left1'].toFixed(2) + '</strong></td>' +
                        '<td><strong>' + totals['left2'].toFixed(2) + '</strong></td>' +
                        '<td class="blue-text"><strong>' + totals['credit1'].toFixed(2) + '</strong></td>' +
                        '<td><strong>' + totals['credit2'].toFixed(2) + '</strong></td>' +
                        '<td class="blue-text"><strong>' + totals['interest1'].toFixed(2) + '</strong></td>' +
                        '<td><strong>' + totals['interest2'].toFixed(2) + '</strong></td>' +
                        '<td class="blue-text"><strong>' + totals['return1'].toFixed(2) + '</strong></td>' +
                        '<td><strong>' + totals['return2'].toFixed(2) + '</strong></td>' +
                        '<td class="' + colorClass(returnValue) + '"><strong>' + module(returnValue).toFixed(2) + '</td>' +
                        '</tr></tfoot>';
                }

                function renderRow(month, data1, data2) {
                    var returnValue = data1['return'] - data2['return'];
                    return '<tr>' +
                        '<td><strong>' + (month + 1) + '</strong></td>' +
                        '<td class="blue-text"><strong>' + data1['left'].toFixed(2) + '</strong></td>' +
                        '<td><strong>' + data2['left'].toFixed(2) + '</strong></td>' +
                        '<td class="blue-text"><strong>' + data1['credit'].toFixed(2) + '</strong></td>' +
                        '<td><strong>' + data2['credit'].toFixed(2) + '</strong></td>' +
                        '<td class="blue-text"><strong>' + data1['interest'].toFixed(2) + '</strong></td>' +
                        '<td><strong>' + data2['interest'].toFixed(2) + '</strong></td>' +
                        '<td class="blue-text"><strong>' + data1['return'].toFixed(2) + '</strong></td>' +
                        '<td><strong>' + data2['return'].toFixed(2) + '</strong></td>' +
                        '<td class="' + colorClass(returnValue) + '"><strong>' + module(returnValue).toFixed(2) + '</td>' +
                        '</tr>';
                }

                function colorClass(value) {
                    return value >= 0 ? 'green-text' : 'red-text';
                }

                function module(value) {
                    return value < 0 ? value * (-1) : value;
                }

                $('#calculate-button').on('click', function() {
                    renderTable();
                });

                $('#own_percentage').on('input', function() {
                    var sumInput = $('#sum');
                    ownPercentage = parseInt($(this).val().trim()) || 15;
                    sumInput.val((price * (100 - ownPercentage) / 100).toFixed());
                });

                renderTable();
            }
        }
    };
})(jQuery);
