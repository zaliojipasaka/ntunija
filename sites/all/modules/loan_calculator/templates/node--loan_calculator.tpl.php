<?php
/**
 * @var array $content Page content values array
 */
//echo $content['price'];
?>

<p class="loan-text">
    <strong>Paskolos skaičiuoklė</strong> apskaičiuoja paskolos mokėjimo grafiką nurodytam paskolos terminui. Visu
    paskolos atidavimo bankui laikotarpiu svarbu ne tik mažos palūkanos, bet ir <span>sutarties sąlygų, mokesčių, papildomų sąlygų visuma</span>.
</p>
<hr/>
<div id="loan-calculator-fields">
    <div class="form-header">
        <div class="wrapper-left">
            Objekto kaina: <strong><?php echo $content['price']; ?> Eur.</strong>
            &nbsp; Savo lėšomis dengiama dalis (%): <span class="help-bullet">i</span>
            <input id="own_percentage" class="form-text" style="width:90px;" type="text" name="own_percentage" value="15">
            <input id="price" type="hidden" value="<?php echo $content['price']; ?>">
        </div>
        <div class="wrapper-right back-button">
            &lt; <a href="#">Grįžti į objekto skelbimą</a>
        </div>
        <div class="clear"></div>
    </div>
    <hr/>
    <div class="form-content">
        <div class="panel-first wrapper-left">
            Kredito suma (Lt): <input id="sum" type="text" class="form-text" style="width:90px;" value="<?php echo (intval($content['price']) * 0.85); ?>"><br/>
            Laikotarpis (m.): <input id="period" type="text" class="form-text" style="width:90px;" value="25">
        </div>
        <div class="panel-second wrapper-left">
            Palūkanų norma (%): <input id="percent_1" type="text" class="form-text" style="width:90px;" value="2"><br/>
            Palūkanų norma <span class="green-text">palyginimui</span> (%): <input id="percent_2" type="text" class="form-text" style="width:90px;" value="2.5">
        </div>
        <div class="panel-third wrapper-left">
            <select id="type_1" style="width:156px;" class="chosen-select form-select" title="Mokėjimo būdas">
                <option value="1" selected>Kintantis (linijinis)</option>
                <option value="2">Pastovus (anuitetas)</option>
            </select><br/>
            <select id="type_2" style="width:156px;" class="chosen-select form-select" title="Mokėjimo būdas">
                <option value="1" selected>Kintantis (linijinis)</option>
                <option value="2">Pastovus (anuitetas)</option>
            </select>
        </div>
        <div class="clear"></div>
        <div id="calculate-button" class="blue-button">Skaičiuoti <span>&nbsp;</span></div>
    </div>
    <h1 class="title" id="payment-table-title">Orientacinis mokėjimų grafikas</h1>
    <div id="payment-table"></div>
    <div id="disclimer">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Apskaičiuotas mokėjimų grafikas yra orientacinis, rezultatas priklauso nuo palūkanų normos. <span class="green-text">Įsidėmėtina, kad net ir nedidelis palūkanų skirtumas turi didelę įtaką galutinei sumai, kurią sumokame bankui!</span></div>
    <h1 class="title" id="payment-table-title">Rezultatų apibendrinimas</h1>
    <div class="form-content">
        <div class="summary-first wrapper-left">
            <div><strong>Iš viso per</strong> <span class="period"></span> m. esant palūkanų normai:</div><br/>
            <div><span class="percent_1 green-text"></span> % sumokėsite bankui <span class="pay_1 green-text"></span> Eur (<span class="method_1"></span>)</div>
            <div><span class="percent_2 green-text"></span> % sumokėsite bankui <span class="pay_2 green-text"></span> Eur (<span class="method_2"></span>)</div><br/>
            <div><strong>Sužinokite savo būsto kainą nemokamai!</strong></div>
        </div>
        <div class="summary-second wrapper-left">
            <div>Skirtumas per visą laikotarpį: <span class="total"></span> Eur</div><br/>
            <div>Per metus: <span class="year"></span> Eur</div>
            <div>Per mėnesį: <span class="month"></span> Eur</div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="form-content request">
        <p>Jeigu iš mūsų siūlomų objektų neišsirinkote Jums tinkančio pasiūlymo, kreipkitės dėl optimaliausių sąlygų gaunant kreditą.</p>
        <div class="summary-first wrapper-left"></div>
        <div class="summary-second wrapper-left"></div>
        <div class="clear"></div>
        <p><span>Prašome užpildyti visus (<span class="red-text">*</span>) pažymėtus laukus!</p>
    </div>
</div>
