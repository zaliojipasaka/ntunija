<?php

/**
 * @file
 * Custom simple view template to display rows in a grid with added advertisement.
 *
 * - $rows contains a nested array of rows. Each row contains an array of
 *   columns.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
<?php endif; ?>
<table class="<?php print $class; ?>"<?php print $attributes; ?>>
    <?php if (!empty($caption)) : ?>
        <caption><?php print $caption; ?></caption>
    <?php endif; ?>

    <tbody>
    <?php foreach ($rows as $row_number => $columns): ?>
        <tr <?php if ($row_classes[$row_number]) { print 'class="' . $row_classes[$row_number] .'"';  } ?>>
            <?php foreach ($columns as $column_number => $item): ?>
                <td <?php if ($column_classes[$row_number][$column_number]) { print 'class="' . $column_classes[$row_number][$column_number] .'"';  } ?>>
                    <?php print $item; ?>
                </td>
                <?php if($row_number == (count($rows) -1) && $column_number == (count($columns) - 1)): ?>
                    <?php if($column_number == 2): ?>
                        </tr>
                        <tr>
                        <td class="col-1 col-first">
                    <?php else: ?>
                        <td>
                    <?php endif; ?>
                        <div class="agent-adv views-field-field-photo">
                            <img class="agent-adv-icon" src="/themes/bartik/images/agent-adv-icon.png">
                            <div class="agent-adv-text">
                                Esi darbšti, dinamiška, energinga asmenybė? Kviečiame prisijungti prie mūsų kolektyvo!
                            </div>
                            <div class="clear"></div>
                            <div class="agent-adv-email">
                                CV ir motyvacinį laišką siųsk el. paštu
                                info@ntunija.lt
                            </div>
                        </div>
                    </td>
                <?php endif; ?>
            <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
