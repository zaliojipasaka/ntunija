<?php

/**
 * @file
 * Custom simple view template to display a list of rows and advertisement.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
    <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
    <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
        <?php print $row; ?>
    </div>
<?php endforeach; ?>
<div class="list-advertisement">
    <form id="object-adv-form" method="POST" action="/">
        <div class="object-adv-image">
            <img src="/themes/bartik/images/object_adv_image.png" height="150" width="235">
        </div>
        <div class="object-adv-fields">
            <input class="object-adv-address" oninvalid="this.setCustomValidity('Įveskite adresą.')" required type="text" name="address" placeholder="Jūsų parduodamo objekto adresas">
            <div class="object-adv-inner">
                <span class="triangle-icon">
                    Kambarių sk.: <input class="object-adv-rooms" name="rooms" type="text">
                </span>
                <span class="triangle-icon">
                    Plotas: <input class="object-adv-area" name="area" type="text">
                </span>
                <span class="triangle-icon">
                    Aukštas: <input class="object-adv-floor" name="floor" type="text">
                </span>
            </div>
            <div class="object-adv-inner">
                <span class="object-adv-price-wrapper view-field-price">
                    <span class="icon">&nbsp;</span>
                    <input type="text" name="price" placeholder="Kaina" required oninvalid="this.setCustomValidity('Įveskite kainą.')">
                </span>
                <span class="object-adv-phone-wrapper">
                    <span class="icon">&nbsp;</span>
                    <input type="text" name="phone" placeholder="Tel. numeris" required oninvalid="this.setCustomValidity('Įveskite telefono numerį.')">
                </span>
                <span class="object-adv-email-wrapper">
                    <span class="icon">&nbsp;</span>
                    <input type="text" name="email" placeholder="El. paštas" required oninvalid="this.setCustomValidity('Įveskite el. paštą.')">
                </span>
                <div class="clear"></div>
            </div>
        </div>
        <div class="object-adv-submit">
            <div class="object-adv-text"></div>
            <input type="submit" value="" class="object-adv-button">
        </div>
        <div class="clear"></div>
    </form>
</div>
